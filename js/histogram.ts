export enum ColorChannels {
    RED = 0,
    GREEN = 1,
    BLUE = 2,
    BRIGHT = 3,
}

export default function buildHistograms(imageData: ImageData, rDiv: HTMLElement, gDiv: HTMLElement, bDiv: HTMLElement, brDiv: HTMLElement): void {
    const histogramStrR = histogramRGB(ColorChannels.RED, imageData, rDiv.clientWidth, rDiv.clientHeight/2);
    const histogramStrB = histogramRGB(ColorChannels.BLUE, imageData, gDiv.clientWidth, gDiv.clientHeight/2);
    const histogramStrG = histogramRGB(ColorChannels.GREEN, imageData, bDiv.clientWidth, bDiv.clientHeight/2);
    const histogramStrBr = histogramRGB(ColorChannels.BRIGHT, imageData, brDiv.clientWidth, brDiv.clientHeight/2);
    
    rDiv.innerHTML = histogramStrR;
    gDiv.innerHTML = histogramStrG;
    bDiv.innerHTML = histogramStrB;
    brDiv.innerHTML = histogramStrBr;
}

interface ColorFrequencyProperty {
    ColorFreqs : Array<number>,
    MaxFreq : number,
}

function getBright(R : number, G : number, B : number) {
    const result = (0.3 * R) + (0.59 * G) + (0.11 * B);
    return result;
}

const histogramRGB = (channel: ColorChannels, image: ImageData, parentWidth: number, parentHeight: number): string => {
    let colorFreq:ColorFrequencyProperty;
    if (channel != ColorChannels.BRIGHT) {
	colorFreq = getColorFreq(channel, image);
    } else {
	colorFreq = getBrightFreq(image);
    }
    
    const histogram = createHistogram(channel, colorFreq, parentWidth, parentHeight);
    
    return histogram;
}

function getBrightFreq(image: ImageData): ColorFrequencyProperty {
    const colorFreq = Array(256).fill(0);

    const len = image.data.length;

    for (let i = 0; i < len; i+=4) {
	const brightness = getBright(image.data[i], image.data[i+1], image.data[i+2]);
	colorFreq[image.data[i]]+=brightness;
    }

    const maxFreq = Math.max.apply(null, colorFreq);

    return { ColorFreqs: colorFreq, MaxFreq: maxFreq };
}

function getColorFreq(channel: ColorChannels, image: ImageData): ColorFrequencyProperty {
    const start = channel;
       
    const colorFreq = Array(256).fill(0);
    
    const len = image.data.length;
    
    for (let i = start; i < len; i += 4) {
	colorFreq[image.data[i]]++;	
    }

    const maxFreq = Math.max.apply(null, colorFreq);
    
    const result: ColorFrequencyProperty = { ColorFreqs: colorFreq, MaxFreq: maxFreq };
    
    return result;
}

function createHistogram(channel: ColorChannels, colorFreqProp: ColorFrequencyProperty, svgWidth: number, svgCenterHeight: number) : string {
    const histWidth = svgWidth;
    const histHeight = svgWidth * 0.5;
    const columnWidth = 2;
    const pixelsPerUnit = histHeight / colorFreqProp.MaxFreq;

    let hexColor;
    let x = 0;
    let columnHeight;

    let svgstring = '';

    for (let i = 0; i < 256; i++) {
	hexColor = i.toString(16).padStart(2, "0");
	
	switch (channel) {
	    case ColorChannels.RED:
		hexColor = "#" + hexColor + "0000";
		break;
	    case ColorChannels.GREEN:
		hexColor = "#00" + hexColor + "00";
		break;
	    case ColorChannels.BLUE:
		hexColor = "#0000" + hexColor;
		break;
	    case ColorChannels.BRIGHT:
		hexColor = "#" + hexColor + hexColor + hexColor;
		break;
	}
	
	columnHeight = colorFreqProp.ColorFreqs[i] * pixelsPerUnit;
	
	svgstring += `<rect fill='${hexColor}' stroke='${hexColor}' stroke-width='0.25px' width='${columnWidth}' height='${columnHeight}' y='${svgCenterHeight - columnHeight}' x='${x}' />\n`;
	
	x += columnWidth;
    }

    return svgstring;
}
