import { Observer } from './Observable';
import ImageUtil, { ImagePosition } from './ImageUtil';



export default class CanvasHandler implements Observer<File> {
    private m_build_histogram_cb: Function;
    private m_canvas: HTMLCanvasElement;
    private m_image_file: File | undefined;
    private m_parent: HTMLDivElement;
    private m_image_position?: ImagePosition;
    
    constructor(parent: HTMLDivElement, canvas: HTMLCanvasElement, buildHistogramCb: Function) {
	this.m_parent = parent;
	this.m_canvas = canvas;
	this.m_build_histogram_cb = buildHistogramCb;
	this.handleResize();
    }

    public getContext(): CanvasRenderingContext2D {
	const ctx = this.m_canvas.getContext("2d");
	if (!ctx) {
	    throw Error("Can't get 2D canvas context!");
	}
	return ctx;
    }

    public getCanvas(): HTMLCanvasElement {
	return this.m_canvas;
    }

    public getCanvasWidth(): number {
	return this.m_canvas.width;
    }
    
    private setCanvasWidth(newWidth: number) {
	this.m_canvas.setAttribute("width", newWidth.toString());
    }

    public getCanvasHeight(): number {
	return this.m_canvas.height;
    }    
    
    private setCanvasHeight(newHeight: number) {
	this.m_canvas.setAttribute("height", newHeight.toString());
    }
    
    public clearCanvas(): void {
	this.getContext().clearRect(0, 0, this.getCanvasWidth(), this.getCanvasHeight());
    }    
    
    onNotify(newValue: File): void {
	this.m_image_file = newValue;
	this.redrawImage();
    }
    
    private handleResize(): void {
	console.log("handleResize");
	this.setCanvasWidth(this.m_parent.clientWidth);
	this.setCanvasHeight(this.m_parent.clientHeight);
	this.redrawImage();
    }
    
    public redrawImage() : void {
	if (this.m_image_file) {
	    ImageUtil.createImageFromFile(this.m_image_file)
		.then((img) => {
		    this.clearCanvas();
		    this.m_image_position = ImageUtil.adjustImagePositionToCanvasSize(img, this.getCanvasWidth(), this.getCanvasHeight());
		    this.drawOnCanvas(img);
		    this.m_build_histogram_cb(this.getImageData());
		})
		.catch((err) => console.error(err));
	}
    }    
       
    private drawOnCanvas(img: HTMLImageElement): void {
	if (img) {
	    this.getContext().drawImage(img, this.m_image_position!.xOffset, this.m_image_position!.yOffset, this.m_image_position!.width, this.m_image_position!.height);
	}
    }

    public getImageData(): ImageData {

	if (!this.m_image_position) {
	    throw Error("File is not set, probably");
	}
	
	return this.getContext().getImageData(this.m_image_position!.xOffset, this.m_image_position!.yOffset, this.m_image_position!.width, this.m_image_position!.height);
    }

    public putImageData(imageData: ImageData): void {
	this.getContext().putImageData(imageData, this.m_image_position!.xOffset, this.m_image_position!.yOffset);
	this.m_build_histogram_cb(imageData);
    }
}

