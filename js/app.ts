import buildHistograms from './histogram';
import './ImageInputHandler';
import ImageInputHandler from './ImageInputHandler';
import CanvasHandler from './CanvasHandler';
import ImageUtil from './ImageUtil';

document.addEventListener("DOMContentLoaded", (e: Event) => {
    e.preventDefault();
    console.log("DOMContentLoaded");
    init();
});

let imgInputHandler: ImageInputHandler;
let canvasHandler: CanvasHandler;
let selectBtn: HTMLButtonElement;
let linearContrastBtn: HTMLButtonElement;
let originalBtn: HTMLButtonElement;
let matrixBtn: HTMLButtonElement;
let conturBtn: HTMLButtonElement;
let posterizeBtn: HTMLButtonElement;

let redHistSvg: HTMLElement; 
let greenHistSvg: HTMLElement; 
let blueHistSvg: HTMLElement; 
let brightHistSvg: HTMLElement; 
    
function init(): void {

    redHistSvg = getElementByIdOrThrowError<HTMLElement>("red-hist");
    greenHistSvg = getElementByIdOrThrowError<HTMLElement>("green-hist");
    blueHistSvg = getElementByIdOrThrowError<HTMLElement>("blue-hist");
    brightHistSvg = getElementByIdOrThrowError<HTMLElement>("bright-hist");
    
    const fileInputDOM = getElementByIdOrThrowError<HTMLInputElement>("file-input");
           
    imgInputHandler = new ImageInputHandler(fileInputDOM);

    const imgCanvasContainerDOM = getElementByIdOrThrowError<HTMLDivElement>("canvas-container");
    const imgCanvasDOM = getElementByIdOrThrowError<HTMLCanvasElement>("image-canvas");

    canvasHandler = new CanvasHandler(imgCanvasContainerDOM, imgCanvasDOM, (imageData: ImageData) => {
	buildHistograms(imageData, redHistSvg, greenHistSvg, blueHistSvg, brightHistSvg);
    });

    imgInputHandler.m_observable.addObserver(canvasHandler);

    selectBtn = getElementByIdOrThrowError<HTMLButtonElement>("select-btn");
    selectBtn.addEventListener("click", (e: Event) => {
	e.preventDefault();
	fileInputDOM.click();
    });

    linearContrastBtn = getElementByIdOrThrowError<HTMLButtonElement>("linear-contrast-btn");
    linearContrastBtn.addEventListener("click", (e: Event) => {
	e.preventDefault();
	canvasHandler.putImageData(ImageUtil.linearContrast(canvasHandler.getImageData(), 0.2, 0.8));
    });

    originalBtn = getElementByIdOrThrowError<HTMLButtonElement>("original-btn");
    originalBtn.onmousedown = (e: Event) => {
	e.preventDefault();
	const oldImageData = canvasHandler.getImageData();
	originalBtn.onmouseup = (e: Event) => {
	    e.preventDefault();
	    canvasHandler.putImageData(oldImageData);
	    originalBtn.onmouseup = null;
	};
	canvasHandler.redrawImage();
    }

    matrixBtn = getElementByIdOrThrowError<HTMLButtonElement>("matrix-btn");
    matrixBtn.onmousedown = (e:Event) => {
	e.preventDefault();
	const oldImageData = canvasHandler.getImageData();
	matrixBtn.onmouseup = (e: Event) => {
	    e.preventDefault();
	    canvasHandler.putImageData(oldImageData);
	    matrixBtn.onmouseup = null;
	}
	canvasHandler.putImageData(ImageUtil.applyMatrix(canvasHandler.getImageData(),
							 [[0.5, 0.75, 0.5],
							  [0.75, 1, 0.75],
							  [0.5, 0.75, 0.5]], 3, 6));
    }

    conturBtn = getElementByIdOrThrowError<HTMLButtonElement>("contur-btn");
    conturBtn.onmousedown = (e:Event) => {
	e.preventDefault();
	const oldImageData = canvasHandler.getImageData();
	conturBtn.onmouseup = (e:Event) => {
	    e.preventDefault();
	    canvasHandler.putImageData(oldImageData);
	    conturBtn.onmouseup = null;
	}
	canvasHandler.putImageData(ImageUtil.sobel(canvasHandler.getImageData()));
    }

    posterizeBtn = getElementByIdOrThrowError<HTMLButtonElement>("posterize-btn");
    posterizeBtn.addEventListener("click", (e: Event) => {
	e.preventDefault();
	canvasHandler.putImageData(ImageUtil.posterize(canvasHandler.getImageData(), 4));
    });
			  
    main();
}

function main() {
    console.log("successfully init");
}

function getElementByIdOrThrowError<T extends HTMLElement>(id: string): T {
    const res = document.getElementById(id);

    if (!res) {
	throw Error(`Can't get HTMLElement by id: ${id}`);
    }

    return res as T;
}


