export default class Observable<T extends Observer<V>, V> {
    private m_observers: Array<T> = new Array();
    private m_value: V | undefined;

    public addObserver(observer: T): void {
	this.m_observers.push(observer);
    }

    public removeObserver(observer: T): void {
	for (var i = 0; i < this.m_observers.length; i++) {
	    const _observer = this.m_observers[i];
	    if (_observer === observer) {
		this.m_observers.splice(i, 1);
	    }
	}
    }

    public getValue(): V | undefined {
	return this.m_value;
    }

    public setValue(newValue: V) {
	this.m_value = newValue;
	this.notifyObservers();
    }

    private notifyObservers() {
	if (this.m_value) {
	    for (var i = 0; i < this.m_observers.length; i++) {
		this.m_observers[i].onNotify(this.m_value);
	    }
	}
    }
}

export interface Observer<V> {
    onNotify(newValue : V) : void;
}
