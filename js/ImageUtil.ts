export interface ImagePosition {
    xOffset: number,
    yOffset: number,
    width: number,
    height: number,
}

interface Color {
    red: number;
    green: number;
    blue: number;
}

export class ColorMinMax {
    private min: number;
    private max: number;
    
    constructor(values: any) {
	var max = Number.MIN_VALUE, min = Number.MAX_VALUE;
        for (var i = 0, len=values.length; i < len; i++) {
            if (values[i] > max) max = values[i];
            if (values[i] < min) min = values[i];
        }
	this.min = min;
	this.max = max;
    }
    
    
    public getMin(): number {
	return this.min;
    }
    
    public getMax(): number {
	return this.max;
    }
    
}

export default class ImageUtil {

    public static async createImageFromFile(file: File) : Promise<any> {
	return new Promise((resolve, reject) => {
	    if (!file) reject(new Error("No file given"));
	    const img = new Image;
	    img.onload = () => resolve(img);
	    img.onerror = (err) => reject(err);
	    img.src = URL.createObjectURL(file);
	});
    }
    
    public static adjustImagePositionToCanvasSize(img: HTMLImageElement, maxWidth: number, maxHeight: number): ImagePosition{
	const srcWidth = img.width as number;
	const srcHeight = img.height as number;
	const srcAspectRatio = srcWidth / srcHeight;
	
	let newWidth = maxWidth;
	let newHeight = newWidth / srcAspectRatio;
	
	if (newHeight > maxHeight) {
	    newHeight = maxHeight;
	    newWidth = newHeight * srcAspectRatio;
	}
	
	const xOffset = newWidth < maxWidth ? ((maxWidth - newWidth) / 2) : 0;
	const yOffset = newHeight < maxHeight ? ((maxHeight - newHeight) / 2) : 0;
	
	return {
	    xOffset,
	    yOffset,
	    width: newWidth,
	    height: newHeight,
	}
    }

    public static linearContrast(oldImageData: ImageData, a: number, b: number): ImageData {
	//	color = 255 * (получитьцветпикселя– minColor)/(maxColor - MinColor)
	const formula = (X: number, min: number, max: number) => 256 * ((X - min)) / (max - min);

	let Rs: number[] = [];
	let Gs: number[] = [];
	let Bs: number[] = [];
	
	for (let i = 0; i < oldImageData.data.length; i += 4) {
	    Rs.push(oldImageData.data[i]);
	    Gs.push(oldImageData.data[i+1]);
	    Bs.push(oldImageData.data[i+2]);
	}
	
	const rColorMinMax = new ColorMinMax(Rs);
	const gColorMinMax = new ColorMinMax(Gs);
	const bColorMinMax = new ColorMinMax(Bs);

	for (let i = 0; i < oldImageData.data.length; i += 4) {
	    oldImageData.data[i] = formula(oldImageData.data[i], rColorMinMax.getMin(), rColorMinMax.getMax());
	    oldImageData.data[i+1] = formula(oldImageData.data[i+1], gColorMinMax.getMin(), gColorMinMax.getMax());
	    oldImageData.data[i+2] = formula(oldImageData.data[i+2], bColorMinMax.getMin(), bColorMinMax.getMax());
	}

	return oldImageData;
    }

    public static applyMatrix(imageData: ImageData, matrix : number[][], matrixsize: number, div: number = 1): ImageData {
	const matrixSide = Math.round(Math.sqrt(matrix.length));
	const matrixHalfSide = Math.floor(matrixSide / 2);

	const data = imageData.data;
	const srcWidth = imageData.width;
	const srcHeight = imageData.height;
	
	for (let y = 0; y < srcHeight; y++) {
	    for (let x = 0; x < srcWidth; x++) {
		let c = this.convolution(x, y, matrix, matrixsize, imageData, div);

		let loc = (x+y*srcWidth)*4;
		imageData.data[loc] = c.red;
		imageData.data[loc+1] = c.green;
		imageData.data[loc+2] = c.blue;
	    }
	}

	return imageData;
    }

    public static sobel(imageData: ImageData) : ImageData {

	// https://github.com/miguelmota/sobel/blob/master/sobel.js

	const width = imageData.width;
	const height = imageData.height;

	function bindPixelAt(data: Uint8ClampedArray | number[]) {
	    return function(x: number, y: number, i: number = 0) {
		return data[((width * y) + x) * 4 + i];
	    };
	}
	
	const sobelMaskX = [[-1, -2, -1],
			    [0, 0, 0],
			    [1, 2, 1]];
	
	const sobelMaskY = [[-1, 0, 1],
			    [-2, 0, 2],
			    [-1, 0, 1]];

	let grayscaleData = [];
	let sobelData = [];

	let pixel_at = bindPixelAt(imageData.data);

	for (let y = 0; y < height; y++) {
	    for (let x = 0; x < width; x++) {
		const r = pixel_at(x, y, 0);
		const g = pixel_at(x, y, 1);
		const b = pixel_at(x, y, 2);

		const avg = (r + g + b) / 3;
		grayscaleData.push(avg, avg, avg, 255);
	    }
	}

	pixel_at = bindPixelAt(grayscaleData);

	const grayScaleImageData = new ImageData(new Uint8ClampedArray(grayscaleData), width, height);

	for (let y = 0; y < height; y++) {
	    for (let x = 0; x < width; x++) {

		const cX = this.convolution(x, y, sobelMaskX, 3, grayScaleImageData);

		const cY = this.convolution(x, y, sobelMaskY, 3, grayScaleImageData);
		
		const magnitude = Math.sqrt((cX.red * cX.red) + (cY.red * cY.red))>>>0;
		sobelData.push(magnitude, magnitude, magnitude, 255);
	    }
	}

	return new ImageData(new Uint8ClampedArray(sobelData), width, height);
    }

    public static posterize(imageData: ImageData, level: number): ImageData {

	const numLevels = this.constrain(level, 2, 256);
	const numAreas = 256 / numLevels;
	const numValues = 256 / (numLevels - 1);
		
	for (let y = 0; y < imageData.height; y++) {
	    for (let x = 0; x < imageData.width; x++) {
		let loc = (x+y*imageData.width)*4;
		imageData.data[loc] = numValues * ((imageData.data[loc] / numAreas)>>0);
		imageData.data[loc+1] = numValues * ((imageData.data[loc+1] / numAreas)>>0);
		imageData.data[loc+2] = numValues * ((imageData.data[loc+2] / numAreas)>>0);
	    }
	}
	
	
	return imageData;
    }
    
    private static convolution(x:number, y:number, matrix:number[][], matrixsize:number, img: ImageData, div: number = 1) : Color {
	let rtotal = 0.0;
	let gtotal = 0.0;
	let btotal = 0.0;

	const offset = Math.floor(matrixsize / 2);

	for (let i = 0; i < matrixsize; i++) {
	    for (let j = 0; j < matrixsize; j++) {
		const xloc = (x + i - offset);
		const yloc = (y + j - offset);
		let loc = (xloc + img.width * yloc) * 4;

		loc = this.constrain(loc, 0, img.data.length - 1);

		rtotal += (img.data[loc]) * matrix[i][j] * (1/div);
		gtotal += (img.data[loc+1]) * matrix[i][j] * (1/div);
		btotal += (img.data[loc+2]) * matrix[i][j] * (1/div);
	    }
	}

	rtotal = this.constrain(rtotal);
	gtotal = this.constrain(gtotal);
	btotal = this.constrain(btotal);
	
	return {red:rtotal,green:gtotal,blue:btotal};
    }

    private static constrain(color: number, min: number = 0, max: number = 255) : number {
	return Math.max(Math.min(color, max), min);
    }
}
