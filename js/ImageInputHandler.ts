import Observable, { Observer  } from './Observable';

export default class ImageInputHandler {

    private m_input: HTMLInputElement;
    public m_observable: Observable<Observer<File>, File> = new Observable();
            
    constructor(input: HTMLInputElement) {
	this.m_input = input;

	this.m_input.addEventListener("change", (e) => {
	    e.preventDefault();
	    if (this.m_input) {
		if (this.m_input.files) {
		    if (this.m_input.files.length > 0) {
			this.m_observable.setValue(this.m_input.files[0]);
		    }
		}
	    }
	});
    }

    public hasImage(): boolean {
	if (this.m_input === null) {
	    return false;
	} else {
	    return this.m_input.files?.length === 1;
	}
    }
}
